# Backend em Python com FASTapi

Este é um projeto de backend em Python usando o framework FASTapi. Ele permite a criação de APIs de maneira rápida e fácil.

## Instalação
1. Clone este repositório em sua máquina local.
2. Instale as dependências com o comando pip install -r requirements.txt.
3. Executando o servidor

Para executar o servidor, navegue até o diretório raiz do projeto e execute o seguinte comando:

```
uvicorn main:app --reload
```

Isso iniciará o servidor e permitirá que você acesse a API em http://localhost:8000.

## Documentação da API
Para visualizar a documentação da API, acesse http://localhost:8000/docs. Isso abrirá o Swagger UI, que permite que você visualize e teste os endpoints da API.

## Contribuindo
Se você quiser contribuir para este projeto, sinta-se à vontade para enviar um pull request. Todas as contribuições são bem-vindas!