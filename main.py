from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware


class Item(BaseModel):
    n1: int
    n2: int

app = FastAPI()

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/app/sum")
async def sum(item: Item):
    result = item.n1 + item.n2
    return result

@app.post("/app/subtract")
async def sum(item: Item):
    result = item.n1 - item.n2
    return result

@app.post("/app/multiplication")
async def sum(item: Item):
    result = item.n1 * item.n2
    return result

@app.post("/app/division")
async def sum(item: Item):
    result = item.n1 / item.n2
    return result

@app.post("/")
async def app():
    return {"Mesage":"OK"}
